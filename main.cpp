#include <iostream>
#include <string>
#include <stack>

using namespace std;

// Class Template 
template<typename T>
class Stack {
private:
    stack<T> data; // Internal stack to store elements

public:
    // Pushing (Adding) element onto the stack
    void push(const T& value) {
        data.push(value);
    }

    // Poping (Removing) the top element from the stack 
    void pop() {
        if (!data.empty()) {
            data.pop();
        }
    }

    // Getting (Fetching) the top element of the stack
    T top() const {
        if (!data.empty()) {
            return data.top();
        }
        // Return default, if stack == empty
        return T();
    }

    // Returns True: if Stack == Empty. Otherwise, False.
    bool isEmpty() const {
        return data.empty();
    }
};

int main() {
    // Use Case 01: Browser History
    Stack<string> browserHistory;

    // Simulate browsing behavior
    browserHistory.push("https://www.zortik.com");
    browserHistory.push("https://whoamirza.vercel.app");
    browserHistory.push("https://www.github.com");

    // Display current page
    cout << "Current Page: " << browserHistory.top() << endl;

    // Back Button Logic
    browserHistory.pop();

    // Display current page after clicking back
    cout << "After clicking back, Current Page: " << browserHistory.top() << endl;

    // I know, this example doesn't fit perfectly for Browser history simulation.
    // Like, what if someone wants go forward again.... This is just for the sake of demonstration and learning.
    // Don't judge please :)))
    
    return 0;
}
