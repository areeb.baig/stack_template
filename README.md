<!DOCTYPE html>
<html lang="en">

<body>

<h1>Template implementation of Stack data structure</h1>

<p>Demonstration using Browser History Simulation Example</p>

<h2>Program Overview</h2>

<p>The program uses a template stack to simulate the behavior of a web browser's navigation history. It allows users to push URLs onto the stack when navigating to new pages and pop URLs from the stack when clicking the "back" button.</p>

<h2>Usage</h2>

<p>To run the program:</p>

<ol>
  <li>Clone the repository:</li>
  <pre>git clone https://gitlab.com/areeb.baig/stack_template.git</pre>

  <li>Compile the program:</li>
  <pre>g++ main.cpp -o stack_template</pre>

  <li>Run the program:</li>
  <pre>./stack_template</pre>
</ol>

<h2>Example</h2>

<p>Here's a sample usage of the program:</p>

<pre>
Current Page: https://www.github.com
After clicking back, Current Page: https://whoamirza.vercel.app
</pre>


</body>
</html>
